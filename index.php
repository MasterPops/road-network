<?php

use App\Classes\Calc;
use App\Classes\Matrix;
use App\Classes\Stairs;

require_once 'vendor/autoload.php';


Stairs::drawStairs(10);

Matrix::handleMatrixWithSumRowsAndCols(2, 3, 9);

echo "\n";

$a = '2';
$b = '15';

$sum = Calc::summation($a, $b);

if ($sum) {
    echo "Сумма чисел $a и $b равна: $sum";
} else {
    echo "Ошибка: одно или оба входных значения не являются числами";
}


