<?php

namespace app\Interfaces;

interface IStairs
{
    public static function drawStairs(int $n): void;
}