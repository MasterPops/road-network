<?php

namespace app\Interfaces;

interface IMatrix
{
    public static function handleMatrixWithSumRowsAndCols(int $n, int $m, int $q): void;
}