<?php

namespace app\Interfaces;

interface ICalc
{
    public static function summation(string $a, string $b): ?int;
}