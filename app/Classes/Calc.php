<?php

namespace App\Classes;

use app\Interfaces\ICalc;

class Calc implements ICalc
{
    public static function summation(string $a, string $b): ?int
    {
        if (ctype_digit($a) && ctype_digit($b)) {
            return bcadd($a, $b);
        }

        return null;
    }
}