<?php

namespace App\Classes;

use app\Interfaces\IStairs;

class Stairs implements IStairs
{
    public static function drawStairs(int $n): void
    {
        $current = 1;

        for ($i = 1; $i <= $n; $i++) {
            for ($j = 1; $j <= $i; $j++) {
                if ($current <= $n) {
                    echo $current;
                    $current++;
                }
            }
            echo "\n";
        }
    }
}