<?php

namespace App\Classes;

use app\Interfaces\IMatrix;

class Matrix implements IMatrix
{
    public static function handleMatrixWithSumRowsAndCols(int $n, int $m, int $q): void
    {
        if ($n * $m > $q) {
            echo "Ошибка: размер матрицы больше количества уникальных чисел";
            return;
        }

        $matrix = [];
        $nums = range(1, $q);
        shuffle($nums);

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $m; $j++) {
                $matrix[$i][$j] = array_shift($nums);
            }
        }

        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $m; $j++) {
                echo $matrix[$i][$j] . "\t";
            }
            echo array_sum($matrix[$i]) . "\n";
        }

        for ($j = 0; $j < $m; $j++) {
            $colSum = 0;
            for ($i = 0; $i < $n; $i++) {
                $colSum += $matrix[$i][$j];
            }
            echo $colSum . "\t";
        }
        echo "\n";
    }
}