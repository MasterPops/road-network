<?php

class Loader
{
    public static function autoload($className): void
    {
        $filename = __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
        include($filename);
    }
}

spl_autoload_register('Loader::autoload');